#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from GameObject import GameObject
from ezha.view import Camera

class Tile( GameObject ):
    def __init__( self ):
        super( Tile, self ).__init__()

    def Draw( self, options ):        
        posRect = self.drawable.GetPosition()
        if ( "camera" in options ):
            posRect = options["camera"].GetAdjustedRect( self.drawable.GetPosition() )
            
        if ( "window" in options and self.drawable.GetPosition() is not None ):
            options["window"].blit( self.drawable.GetImage(), posRect, self.drawable.GetFrame() )
