#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import copy

##  A Collidable object is a rectangle that marks where on a sprite
#   it is "collidable". This is used for basic bounding-box collision detection.
class Collidable( object ):
    ##  Initialize the Collidable object
    ##  @param  options.collisionRect         Initialize the rectangle
    def __init__( self, options ):
        self.collisionRect = pygame.Rect( 0, 0, 16, 16 )

        self.Setup( options )

    ##  Explicitly set up the Collidable object
    ##  @param  options.posRect         Initialize the item's position
    ##  @param  options.frameRect       Initialize the item's frame
    ##  @param  options.image           Initialize the item's image
    def Setup( self, options ):
        if ( "collisionRect" in options ):
            self.SetCollisionRect( options )
            
    ##  Set the collision rectangle region pygame.Rect( x, y, width, height )
    ##  @param  options.collisionRect   Given a sprite's top-left corner,
    #   x is inwards from the left corner, y is inwards from the top corner,
    #   width and height dictate how big the rectangle is
    def SetCollisionRect( self, options ):
        if ( "collisionRect" in options ):
            self.collisionRect = copy.copy( options["collisionRect"] )

    ##  Get the collision rectangle
    def GetCollisionRect( self ):
        return self.collisionRect
