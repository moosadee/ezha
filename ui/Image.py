#!/usr/bin/python
# -*- coding: utf-8 -*-

# Image properties:
#   image_texture       Image to draw
#   image_position      Where to draw image
#   image_blitrect      Rectangle position on the image to draw
#   image_effect        UNIMPLEMENTED

from copy import copy

class Image:
    def __init__( self, options=None ):
        self.options = {}
        self.image = None
        self.imagePosition = None
        self.imageBlitRect = None
        self.effect = None
        self.effectCounter = 0
        self.effectCounterMax = 0

        if ( options is not None ):
            self.Setup( options )
        
    def Setup( self, options ):
        # Store all options
        self.options = copy( options )
            
        if ( "image_texture" in self.options ):
            self.image = self.options["image_texture"];
            
        if ( "image_position" in self.options ):
            self.imagePosition = self.options["image_position"]
    
        if ( "image_blitrect" in self.options ):
            self.imageBlitRect = self.options["image_blitrect"]
    
        if ( "image_effect" in self.options ):
            self.effect = self.options["image_effect"]
            self.effectCounterMax = 20
            
    def ChangeImage( self, newTexture ):
        self.image = newTexture;
            
    def Update( self ):
        a = 2
        #if ( self.effect == "bob" ):
    
    def Draw( self, windowSurface ):
        #self.Update()
        
        # Draw the image        
        if ( self.image is not None and self.imageBlitRect is not None and self.imagePosition is not None ):
            windowSurface.blit( self.image, self.imagePosition, self.imageBlitRect )
            
        elif ( self.image is not None and self.imagePosition is not None ):
            windowSurface.blit( self.image, self.imagePosition )
        

