#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

class InventoryItem( object ):
    def __init__( self, properties=None ):
        self.properties = []
        self.name = ""
        self.image = None
        
        if ( properties is not None ):
            self.Setup( properties )
        
    def Setup( self, properties ):
        self.properties = properties
        if "name" in properties: self.name = properties["name"]
        if "image" in properties: self.image = properties["image"]
        
    def SetProperty( self, key, value ):
        self.properties.append( { key : value } )
        
    def GetProperty( self, key ):
        return self.properties[key]
    
    def GetName( self ):
        return self.name
        
    def GetImage( self ):
        return self.image
