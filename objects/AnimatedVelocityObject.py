#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import copy

from ezha.modules import Drawable
from ezha.modules import Collidable
from ezha.modules import Animatable
from ezha.modules import VelocityMovable

class AnimatedVelocityObject( object ):
    def __init__( self, options=None ):
        self.animatable = Animatable( {} )
        self.collidable = Collidable( {} )
        self.velocityMovable = VelocityMovable( {} )

        if ( options is not None ):
            self.Setup( options )

    def Setup( self, options ):
        self.animatable.Setup( options )
        self.collidable.Setup( options )
        self.velocityMovable.Setup( options )

    def Update( self ):
        self.animatable.Update()
        self.velocityMovable.Update()

        # Change position based on velocity
        posRect = self.animatable.GetPosition()
        velocity = self.velocityMovable.GetVelocity()

        posRect.x += velocity[0]
        posRect.y += velocity[1]

        self.animatable.SetPosition( posRect )

    def GetCollisionRegion( self ):
        colRect = copy.copy( self.collidable.GetCollisionRect() )
        posRect = self.drawable.GetPosition()
        colRect.x += posRect.x
        colRect.y += posRect.y
        return colRect
        
    def SetPosition( self, options ):
        self.animatable.SetPosition( options )

    def GetPosition( self ):
        return self.animatable.GetPosition()

    def SetFrame( self, options ):
        self.animatable.SetFrame( options )
        
    def GetFrame( self ):
        return self.animatable.GetFrame()
        
    def SetImage( self, options ):
        self.animatable.SetImage( options )

    def SetAction( self, options ):
        self.animatable.SetAction( options )

    def GetAction( self ):
        return self.animatable.GetAction()

    def SetCollisionRect( self, options ):
        self.collidable.SetCollisionRect( options )

    def GetCollisionRect( self ):
        return self.collidable.GetCollisionRect()

    def Accelerate( self, options ):
        self.velocityMovable.Accelerate( options )

    def Draw( self, options ):
        self.animatable.Draw( options )

