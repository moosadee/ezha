#!/usr/bin/python
# -*- coding: utf-8 -*-

import math

class Utilities:
    clock = None

    @classmethod
    def SetFPSClock( pyClass, clock ):
        pyClass.clock = clock

    @classmethod
    def GetFPSClock( pyClass ):
        return pyClass.clock

    @classmethod
    def GetDistance( pyClass, rect1, rect2 ):
        xDiff = (rect1.x + rect1.width/2) - (rect2.x + rect2.width/2)
        yDiff = (rect1.y + rect1.height/2) - (rect2.y + rect2.height/2)
        xSq = xDiff * xDiff
        ySq = yDiff * yDiff

        return math.sqrt( xSq + ySq )
        
    @classmethod
    def Get1DDistance( pyClass, pointA, pointB ):
        diff = pointA - pointB
        if ( diff < 0 ):
            return -diff
        return diff
        
    @classmethod
    def GetPointDistance( pyClass, x1, y1, x2, y2 ):
        dx = x2 - x1
        dy = y2 - y1
        return math.sqrt( dx * dx + dy * dy )

    @classmethod
    def RectCollision( pyClass, rect1, rect2 ):
        dist = pyClass.GetDistance( rect1, rect2 )

        return ( rect1.x <= rect2.x + rect2.w and rect1.x + rect1.w >= rect2.x
            and  rect1.y <= rect2.y + rect2.h and rect1.y + rect1.h >= rect2.y )
        
        
