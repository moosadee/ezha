#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import InventoryItem

class Inventory( object ):
    def __init__( self ):
        self.inventory = {}
            
    def AddToInventory( self, key, item, quantity ):
        # Add on to the current quantity
        if ( key in self.inventory ):
            self.inventory[key]["quantity"] += quantity
            
        # Add first of this inventory item
        else:
            self.inventory[key] = { "quantity" : quantity, "itemType" : item }
            
    def AmountInInventory( self, key ):
        if ( key in self.inventory ):
            return self.inventory[key]["quantity"]
        else:
            return 0
    
    def RemoveQuantityFromInventory( self, key, amount ):
        if ( key in self.inventory ):
            self.inventory[key]["quantity"] -= amount
        
    def RemoveAllFromInventory( self, key ):
        if ( key in self.inventory ):
            del self.inventory[key]
        
