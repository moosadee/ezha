import pygame, sys, csv
from pygame.locals import *

class ConfigManager:
    options = {}
    
    @classmethod
    def Load( pyClass ):
        pyClass.options = {}
        
        # Load Helper
        filepath = "config.csv"
        
        with open( filepath, "rb" ) as csvfile:
            contents = csv.reader( csvfile, delimiter=',', quotechar='"' )
            
            for row in contents:
                pyClass.options[ row[0].lower() ] = row[1]
        
        print( str( len( pyClass.options ) ) + " config items loaded" )
        print( pyClass.options )

    @classmethod
    def ChangeValue( pyClass, key, value ):
        pyClass.options[ key ] = value

    @classmethod
    def Save( pyClass ):
        filepath = "config.csv"

        with open( filepath, "w" ) as csvfile:
            for option in pyClass.options:
                csvfile.write( option + "," + str( pyClass.options[option] ) + "\n" )

            csvfile.close()
            print( "Config file updated." )
