import pygame, sys
from pygame.locals import *

from ezha.inventory import InventoryItem

class InventoryManager:
    itemCatalog = {}
    
    @classmethod
    def AddToCatalog( pyClass, key, item ):
        pyClass.itemCatalog[key] = item
        
    @classmethod
    def GetFromCatalog( pyClass, key ):
        return pyClass.itemCatalog[key]
