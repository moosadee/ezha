#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import copy

##  A Drawable object is a static texture that has a position and size
#   on the screen, as well as a "frame" out of a spritesheet for its image
class Drawable( object ):
    ##  Initialize the Drawable object
    ##  @param  options.posRect         Initialize the item's position
    ##  @param  options.frameRect       Initialize the item's frame
    ##  @param  options.image           Initialize the item's image
    def __init__( self, options ):
        self.posRect = pygame.Rect( 0, 0, 16, 16 )
        self.frameRect = pygame.Rect( 0, 0, 16, 16 )
        self.image = None
        self.Setup( options )

    ##  Explicitly set up the Drawable object
    ##  @param  options.posRect         Initialize the item's position
    ##  @param  options.frameRect       Initialize the item's frame
    ##  @param  options.image           Initialize the item's image
    def Setup( self, options ):
        self.SetPosition( options )
        self.SetFrame( options )
        self.SetImage( options )
        self.SetImage( options )

    ##  Set the image of the drawable item, or load it in and store.
    #   @param options.image    The texture to store a reference to     OR
    #   @param options.filepath The path of the file to load as image
    #
    #   Note: Memory-wise, it is best to store all your textures elseware
    #   and have the Drawable point to a reference of that texture,
    #   than to have the Drawable own its texture.
    #   Also, if more than one Drawable object use the same texture,
    #   definitely load the texture externally and just pass it in.
    def SetImage( self, options ):
        if ( "image" in options ):
            self.image = options["image"]
            
        elif ( "filepath" in options ):
            self.image = pygame.image.load( options["filepath"] )

    ##  Get the texture for this object
    def GetImage( self ):
        return self.image

    ##  Set the screen position of the Drawable object
    #   @param  options["posRect"]      The pygame.Rect( x, y, width, height ) of the item to draw. OR
    #   @param  options["x"]            The new x coordinate for the posRect
    #   @param  options["y"]            The new y coordinate for the posRect
    #   width and height correspond to the scaled size on screen.
    def SetPosition( self, options ):        
        if ( "posRect" in options ):
            self.posRect = copy.copy( options["posRect"] )

        # If you only need to update position
        elif ( "x" in options and "y" in options ):
            self.posRect.x = options["x"]
            self.posRect.y = options["y"]

    ##  Return the position pygame.Rect( x, y, width, height ) of the drawable object.
    #   width and height correspond to the scaled size on screen.
    def GetPosition( self ):
        return self.posRect

    ##  Set which portion of the image to display
    #   @param options["frameRect"]     The pygame.Rect ( x, y, width, height ) of the frame.
    #   width and height correspond to the area you're grabbing from the spritesheet.
    def SetFrame( self, options ):
        if ( "frameRect" in options ):
            self.frameRect = copy.copy( options["frameRect"] )

    ##  Returns the current frame pygame.Rect( x, y, width, height ).
    #   width and height correspond to the area you're grabbing from the spritesheet.
    def GetFrame( self ):
        return self.frameRect

    ##  Draw the image to the screen
    #   @param options["window"]       The window we're drawing to
    def Draw( self, options ):
        if ( "window" in options ):
            options["window"].blit( self.image, self.posRect, self.frameRect )

