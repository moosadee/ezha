#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha.utilities import Logger

import copy

class Camera:
    def __init__( self ):
        self.offsetX = 0
        self.offsetY = 0
        self.minX = 0
        self.minY = 0
        self.maxX = 0
        self.maxY = 0
        self.screenWidth = 0
        self.screenHeight = 0

    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight

    def SetRange( self, minX, minY, maxX, maxY ):
        self.minX = minX
        self.maxX = maxX
        self.minY = minY
        self.maxY = maxY

    def ChangeOffset( self, amountX, amountY ):
        self.offsetX += amountX
        self.offsetY += amountY
        self.BindOffset()

    def GetOffsetX( self ):
        return self.offsetX

    def GetOffsetY( self ):
        return self.offsetY

    def CenterOn( self, rect ):
        self.offsetX = rect.x - (rect.w / 2) - (self.screenWidth / 2)
        self.offsetY = rect.y - (rect.h / 2) - (self.screenHeight / 2)
        self.BindOffset()

    def BindOffset( self ):
        if ( self.offsetX < self.minX ):    self.offsetX = self.minX
        elif ( self.offsetX > self.maxX ):  self.offsetX = self.maxX

        if ( self.offsetY < self.minY ):    self.offsetY = self.minY
        elif ( self.offsetY > self.maxY ):  self.offsetY = self.maxY

    def GetAdjustedRect( self, rect ):
        posRect = copy.copy( rect )
        posRect.x -= self.GetOffsetX()
        posRect.y -= self.GetOffsetY()
        return posRect

    def IsRegionVisible( self, rect ):
        return (
            rect.x + rect.w >= self.GetRegionLeft() and     # Item's right edge within screen
            rect.x <= self.GetRegionRight() and             # Item's left edge within screen
            rect.y + rect.h >= self.GetRegionTop() and
            rect.y <= self.GetRegionBottom() )

    def DebugIsRegionVisible( self, rect, window=None ):
        
        #if ( window is not None and self.IsRegionVisible( rect ) ):
            #Logger.Write( "Rect: (" + str( rect.x ) + ", " + str( rect.y ) + ") \t " + str( rect.w ) + " x " + str( rect.h ) )
            #pygame.draw.line( window, pygame.Color( 255, 0, 0 ), ( rect.x, rect.y ), ( rect.x + rect.w, rect.y ) )
            #pygame.draw.line( window, pygame.Color( 255, 0, 0 ), ( rect.x, rect.y + rect.h ), ( rect.x + rect.w, rect.y + rect.h ) )
            #pygame.draw.line( window, pygame.Color( 255, 0, 0 ), ( rect.x, rect.y ), ( rect.x, rect.y + rect.h ) )
            #pygame.draw.line( window, pygame.Color( 255, 0, 0 ), ( rect.x + rect.w, rect.y ), ( rect.x + rect.w, rect.y + rect.h ) )

        return self.IsRegionVisible( rect )

    def GetRegionLeft( self ):
        return self.offsetX

    def GetRegionRight( self ):
        return self.offsetX + self.screenWidth

    def GetRegionTop( self ):
        return self.offsetY

    def GetRegionBottom( self ):
        return self.offsetY + self.screenHeight

    def GetRegionWidth( self ):
        return self.screenWidth

    def GetRegionHeight( self ):
        return self.screenHeight

    def DebugDraw( self, window ):
        x1 = self.GetRegionLeft() - self.GetOffsetX()
        y1 = self.GetRegionTop() - self.GetOffsetY()
        x2 = self.GetRegionRight() - self.GetOffsetX()
        y2 = self.GetRegionBottom() - self.GetOffsetY()
        
        #Logger.Write( "Camera region: " + str( self.GetRegionLeft() ) + ", " + str( self.GetRegionTop() ) + " to " + str( self.GetRegionRight() ) + ", " + str( self.GetRegionBottom() ) )
        pygame.draw.line( window, pygame.Color( 255, 255, 0 ), ( x1, y1 ), ( x2, y1 ) )
        pygame.draw.line( window, pygame.Color( 255, 255, 0 ), ( x1, y2 ), ( x2, y2 ) )
        pygame.draw.line( window, pygame.Color( 255, 255, 0 ), ( x1, y1 ), ( x1, y2 ) )
        pygame.draw.line( window, pygame.Color( 255, 255, 0 ), ( x2, y1 ), ( x2, y2 ) )
