#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import copy

from ezha.modules import Drawable
from ezha.modules import Collidable

class GameObject( object ):
    def __init__( self ):
        self.drawable = Drawable( {} )
        self.collidable = Collidable( {} )

    def Setup( self, options ):
        self.drawable.Setup( options )
        self.collidable.Setup( options )

    def SetPosition( self, options ):
        self.drawable.SetPosition( options )

    def GetPosition( self ):
        return self.drawable.GetPosition()

    def SetFrame( self, options ):
        self.drawable.SetFrame( options )

    def GetFrame( self ):
        return self.drawable.GetFrame()
        
    def SetImage( self, options ):
        self.drawable.SetImage( options )

    def SetCollisionRect( self, options ):
        self.collidable.SetCollisionRect( options )

    def GetCollisionRect( self ):
        return self.collidable.GetCollisionRect()

    def GetCollisionRegion( self ):
        colRect = copy.copy( self.collidable.GetCollisionRect() )
        posRect = self.drawable.GetPosition()
        colRect.x += posRect.x
        colRect.y += posRect.y
        return colRect

    def Draw( self, options ):
        self.drawable.Draw( options )

